let profileContainer = document.querySelector('#profileContainer')
let token = localStorage.getItem("token")



if(!token || token === null) {
        window.location.replace("./login.html")
        alert("Please login first")
    } 
    else {
        fetch('https://cryptic-mountain-32338.herokuapp.com/api/users/details', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
                
            }
        })
        .then(res => {
            return res.json()
        })
        .then(data => {
            console.log(data)

            if(data.isAdmin === false){
            	let enrollmentData = data.enrollments.map(classData => {
            		return `
				<tr>
				   <td>${classData.courseId}</td>
				   <td>${classData.status}</td> 
				</tr> 
				`
            	});
            
            	enrollmentData.join('')

                let userId = data._id
                console.log(userId)

            	profileContainer.innerHTML = `
            	<div class="col-md-7 ml-auto bg-nav3 p-3">
				
					<p class="text-left"><strong>First Name:</strong> ${data.firstName}</p>
					<p class="text-left"><strong>Last Name:</strong> ${data.lastName}</p>
					<p class="text-left"><strong>Email:</strong> ${data.email}</p>
                    <p class="text-left"><strong>Mobile Number:</strong> ${data.mobileNo}</p>
					<button type="submit" class="btn btn-leaf" onclick="userId='${data._id}';updateProfile()" id="edit" value="${data._id}">Update Profile</button>
                    <button type="submit" class="btn btn-leaf" onclick="userId='${data._id}';editPassword()" id="edit" value="${data._id}">Change Password</button>
                    <br/><br/>
                    <p class="text-left"><strong>Class History</strong></p>
                    
					<table class="table">
						<thead>
							<tr>
								<th> CourseID </th>
								<th> Status </th>
							</tr>
							<tbody>
								${enrollmentData}
							</tbody>
						</thead>
					</table>
			</div>
		`
    }else{
    	window.location.replace("./courses.html")
    	alert("Admins don't have a profile")
    }

})
    }