
let token = localStorage.getItem("token")
let courseId = localStorage.getItem("courseId")
let userId = localStorage.getItem("id")

let firstName = document.querySelector("#firstName")
let lastName = document.querySelector("#lastName")
let email = document.querySelector("#email")
let mobileNo = document.querySelector("#mobileNo")
console.log(userId)

fetch(`https://cryptic-mountain-32338.herokuapp.com/api/users/${userId}`)
        .then(res => {
            return res.json()
        })
        .then(data => {
            console.log(data)
            console.log("userID: " + userId)

            firstName.placeholder = data.firstName
            lastName.placeholder = data.lastName
            email.placeholder = data.email
            mobileNo.placeholder = data.mobileNo
            password = data.password

            document.querySelector("#updateUser").addEventListener("submit", (letsedit) => {
                letsedit.preventDefault()

                let newfirstName = firstName.value
                let newlastName = lastName.value
                let newemail = email.value
                let newmobileNo = mobileNo.value
                let newUserId = userId
                let token = localStorage.getItem("token")

                console.log("new: " + newfirstName)
                console.log("new: " + newlastName)
                console.log("new: " + newemail)
                console.log("new: " + newmobileNo)

                fetch(`https://cryptic-mountain-32338.herokuapp.com/api/users/details`, {
                       
                        method:'PUT',
                        headers: {
                            'Content-Type' : 'application/json',
                            'Authorization': `Bearer ${token}`
                            },
                            body: JSON.stringify({
                                userId:userId,
                                firstName: newfirstName,
                                lastName: newlastName,
                                email: newemail,
                                mobileNo: newmobileNo,
                                password : password
                                })

                            })
                            .then(res => { //additional note: res.send function sets the content type to text/html which means that the client will now treat the response as text. it then retunrs a response to the client.
                                return res.json()
                            })
                            .then(data =>{
                                console.log(data)
                                 if (data === true) {
                                    alert('profile successfully updated')
                                  window.location.replace("./profile.html")

                                }else {
                                    alert("something went wrong.")
                                }
                            })
                        })
                        })
    