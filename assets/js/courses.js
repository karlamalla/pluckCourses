

localStorage.removeItem(localStorage.courseId)


let adminButton = document.querySelector('#adminButton')
let courseContainer = document.querySelector('#courseContainer')
let token = localStorage.getItem("token")


       //lets now create a control structure that will allow us to determine if there is data inserted in the given fields
    
if(!token || token === null) {
    fetch ('https://cryptic-mountain-32338.herokuapp.com/api/courses', {
        method: 'GET', 
        headers: {
            'Content-Type': 'application/json' 
        },
        })
                
        .then(res => {
        return res.json()
        })
                
        .then(data => {

        console.log(data)
        let courses = data.map(coursesData => {

            if (coursesData.isActive === true){
            return `
            <div class="col-md-9 my-3 ml-auto">
                <div class="card">
                    <div class="card-body">
                    <table>
                        <tbody>
                            <tr style="border-bottom:solid 1px gray;">
                                <td width="150px">
                                    <h6 class="card-title">${coursesData.name}</h6>
                                </td>
                                <td width="30%">
                                    <h6 class="card-title">Php${coursesData.price}</h6>
                                </td>
                                <td width="30%" vertical-align="center">
                                    <button type="submit" class="btn btn-leaf2" onclick="courseId='${coursesData._id}';e()" id="view" value="${coursesData._id}">View</button>
                                </td>

                            </tr>

                            <tr>

                            <td colspan="3" style="font-size:14px;"><br/>${coursesData.homepageDescription}</td>
                            </tr>
                        <tbody>
                    </table>
                    </div>
                </div>
            </div> ` }  

        })
                
        courses.join('')

        courseContainer.innerHTML = `
            <div class="col-md-9 ml-auto">
            <div class="row">
                
                ${courses}
                                   
            </div>
            </div>`
                    
        }) 

    } else {
        fetch('https://cryptic-mountain-32338.herokuapp.com/api/users/details', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
                
            }
        })
        .then(res => {
            return res.json()
        })
        .then(data => {
            console.log(data)

            if(data.isAdmin === true){



                fetch ('https://cryptic-mountain-32338.herokuapp.com/api/courses', {
                    method: 'GET', 
                    headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${token}`
                    },
                

                })
                .then(res => {
                return res.json()
                })
                .then(data => {
                console.log(data)
                

                adminButton.innerHTML = 
                `<div class="col-md-12">
                <a href="./addCourse.html" class="btn btn-outline-leaf mb-3">add course</a>
                </div>`

                let courses = data.map(coursesData => {

                console.log(coursesData.isActive)

                if (coursesData.isActive === true){

                return `
                <tr>
                <td class="coursedetails">${coursesData.isActive}
                   </td> 
                   <td class="coursedetails">${coursesData.name}
                   </td> 
                   <td class="coursedetails">${coursesData.description}<br/><br/>
                   <button type="submit" class="btn btn-leaf-outline" onclick="courseId='${coursesData._id}';e()" id="view" value="${coursesData._id}">View</button>
                   
                   <button type="submit" class="btn btn-leaf-outline" onclick="courseId='${coursesData._id}';edit()" id="edit" value="${coursesData._id}">Edit</button>
                   <button type="submit" class="btn btn-leaf-outline" onclick="courseId='${coursesData._id}';del()" id="delete" value="${coursesData._id}">Deactivate</button>
                   </td> 
                   <td class="coursedetails">${coursesData.price}</td>
                   <td class="coursedetails">${coursesData.createdOn} <br/>
                   </td>
                </tr> 

                
                `} else{
                    return`
                    <tr>
                <td class="coursedetails">${coursesData.isActive}
                   </td> 
                   <td class="coursedetails">${coursesData.name}
                   </td> 
                   <td class="coursedetails">${coursesData.description}<br/><br/>
                   <button type="submit" class="btn btn-leaf-outline" onclick="courseId='${coursesData._id}';e()" id="view" value="${coursesData._id}">View</button>
                   
                   <button type="submit" class="btn btn-leaf-outline" onclick="courseId='${coursesData._id}';edit()" id="edit" value="${coursesData._id}">Edit</button>
                   <button type="submit" class="btn btn-leaf-outline" onclick="courseId='${coursesData._id}';activate()" id="activate" value="${coursesData._id}">Activate</button>
                   </td> 
                   <td class="coursedetails">${coursesData.price}</td>
                   <td class="coursedetails">${coursesData.createdOn} <br/>
                   </td>
                </tr> `
                }
                });
            
                courseContainer.innerHTML = `
                <div class="col-md-8 bg-nav3 text-center ml-auto p-3">
                    <table class="table">
                        <thead>
                            <tr>
                            <th class="courseheaders"> Is course active? </th>
                                <th class="courseheaders"> Course Name </th>
                                <th class="courseheaders"> Description </th>
                                <th class="courseheaders"> Price </th>
                                <th class="courseheaders"> Created On </th>
                            </tr>
                            <tbody>
                                ${courses}
                            </tbody>
                        </thead>
                    </table>
                </div>`
                    
                }) 
                
                }else{
                    fetch ('https://cryptic-mountain-32338.herokuapp.com/api/courses', {
        method: 'GET', 
        headers: {
            'Content-Type': 'application/json' 
        },
        })
                
        .then(res => {
        return res.json()
        })
                
        .then(data => {

        console.log(data)
        let courses = data.map(coursesData => {
            if (coursesData.isActive === true){
            return `
            <div class="col-md-9 my-3 ml-auto">
                <div class="card">
                    <div class="card-body">
                    <table>
                        <tbody>
                            <tr style="border-bottom:solid 1px gray;">
                                <td width="150px">
                                    <h6 class="card-title">${coursesData.name}</h6>
                                </td>
                                <td width="30%">
                                    <h6 class="card-title">Php${coursesData.price}</h6>
                                </td>
                                <td width="30%" vertical-align="center">
                                    <button type="submit" class="btn btn-leaf2" onclick="courseId='${coursesData._id}';e()" id="view" value="${coursesData._id}">View</button>
                                </td>

                            </tr>

                            <tr>

                            <td colspan="3" style="font-size:14px;"><br/>${coursesData.homepageDescription}</td>
                            </tr>
                        <tbody>
                    </table>
                    </div>
                </div>
            </div>`   
}
        })
                
        courses.join('')

        courseContainer.innerHTML = `
            <div class="col-md-9 text-center ml-auto">
            <div class="row">
                
                ${courses}
                                   
            </div>
            </div>`
                    
        }) 

    }
                })
                //the else branch will run if there us an authentication failure
                
                
}



