
let token = localStorage.getItem("token")
let courseId = localStorage.getItem("courseId")
let userId = localStorage.getItem("id")

let password1 = document.querySelector("#oldpassword")
let password2 = document.querySelector("#password1")
let password3 = document.querySelector("#password2")


console.log(userId)

fetch(`https://cryptic-mountain-32338.herokuapp.com/api/users/${userId}`)
        .then(res => {
            return res.json()
        })
        .then(data => {
            console.log(data)
            console.log(data.password)

            let currentpassword = data.password
            let firstName = data.firstName
            let lastName = data.lastName
            let email = data.email
            let mobileNo = data.mobileNo

            document.querySelector("#updateUser").addEventListener("submit", (letsedit) => {
                letsedit.preventDefault()

                
            let newpassword1 = password2.value
            let newpassword2 = password3.value

            
            if((newpassword1 !== '' && newpassword2 !== '') && (newpassword2 === newpassword1)){

                fetch(`https://cryptic-mountain-32338.herokuapp.com/api/users/details`, {
                       
                        method:'PUT',
                        headers: {
                            'Content-Type' : 'application/json',
                            'Authorization': `Bearer ${token}`
                            },
                            body: JSON.stringify({
                                userId:userId,
                                password:newpassword1,
                                firstName: firstName,
                                lastName: lastName,
                                email: email,
                                mobileNo: mobileNo
                                })

                            })
                            .then(res => { //additional note: res.send function sets the content type to text/html which means that the client will now treat the response as text. it then retunrs a response to the client.
                                return res.json()
                            })
                            .then(data =>{
                                console.log(data)
                                 if (data === true) {
                                   alert("password updated")
                                   window.location.replace("./logout.html")

                                }else {
                                    alert("something went wrong.")
                                }
                            })
                        }else{
                            alert("passwords don't match")
                        }
                        })

            

            })
