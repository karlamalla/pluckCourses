
let courseId = localStorage.getItem("courseId")
let courseEnrollees = document.querySelector('#courseEnrollees')
let token = localStorage.getItem("token")
let userId = localStorage.getItem("id")



console.log(courseId)
                

if(!token || token === null) {

    fetch (`https://cryptic-mountain-32338.herokuapp.com/api/courses/${courseId}`, {
        method: 'GET', 
        headers: {
            'Content-Type': 'application/json' ,
            'Authorization': `Bearer ${token}`
        },
                

        })
                
        .then(res => {
            return res.json()
        })
                
        .then(data => {
        let name = document.querySelector("#courseName").innerHTML = data.name
        let description = document.querySelector("#courseDescription").innerHTML = data.description
        let price = document.querySelector("#coursePrice").innerHTML = data.price
                
                    
        }) 
        
    } 
    else {
        fetch(`https://cryptic-mountain-32338.herokuapp.com/api/users/details`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
                
            }
        })
        .then(res => {
            return res.json()
        })
        .then(data => {
            console.log(data)
            if(data.isAdmin === true){

                fetch(`https://cryptic-mountain-32338.herokuapp.com/api/courses/${courseId}`, {
                    method: 'GET',
                    headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${token}`
                
                    }
                })
                .then(res => {
                return res.json()
                })
                .then(data => {
                //store JWT in the localStorage
                
                let enrollees = data.enrollees.map(classData => {
                    return `
                    <tr>
                    <td><p>${classData.userId}</p><td>
                    <td<p>${classData.enrolledOn}</p></td>
                   </tr>
                
                    `
                    });            
                    
                    courseEnrollees.innerHTML = `
                    <div class ="row">
                      <div class="col-md-12 ml-auto">
                        <div class="row">
                    <div class="col-md-7 ml-auto">
                    <h1 class="jumbotron text-center" style="font-size:50px">${data.name} <br/>enrollees</h1></div>
                    <div class="col-md-7 bg-nav3 ml-auto p-3 mt-5">


                        <table class="table">
                            <thead>
                                <tr>
                                    
                                    <th class="courseheaders"> Student ID </th>
                                    <th class="courseheaders"> Enrolled On </th>
                                    
                                </tr>
                                <tbody>
                                    
                                    

                                    ${enrollees}

                                </tbody>
                            </thead>
                        </table>
                    </div></div></div></div>`
                        
                })
            }else {
                //the else branch will run if there us an authentication failure
                fetch (`https://cryptic-mountain-32338.herokuapp.com/api/courses/${courseId}`, {
                    method: 'GET', 
                    headers: {
                    'Content-Type': 'application/json' ,
                    'Authorization': `Bearer ${token}`
                    },
                

                })
                
                .then(res => {
                return res.json()
                })
                
                .then(data => {
                    
                    let name = document.querySelector("#courseName").innerHTML = data.name
                    let description = document.querySelector("#courseDescription").innerHTML = data.description
                    let price = document.querySelector("#coursePrice").innerHTML = data.price
                    let courseId = data._id
                    let enrollContainer = document.querySelector('#enrollContainer')

                    enrollContainer.innerHTML=
                            `
                            <span id="enrollContainer">
                              <button type="submit" class="btn btn-outline-leaf" id="enrollCourse" onclick="enroll()" value="${courseId}">enroll</button>
                            </span>
                            </div>`
                        })
                   
                   
                }
                    

                    
                        
        })
    }




