

let registerForm = document.querySelector("#registerUser")

registerForm.addEventListener("submit", (e) => {
	e.preventDefault()

	let firstName = document.querySelector("#firstName").value
	let lastName = document.querySelector("#lastName").value
	let mobileNo = document.querySelector("#mobileNo").value
	let email = document.querySelector("#email").value
	let password1 = document.querySelector("#password1").value
	let password2 = document.querySelector("#password2").value

	//validation to enabel the submit button

	if((password1 !== '' && password2 !== '') && (password2 === password1) && (mobileNo.length === 11)) {
		fetch ('https://cryptic-mountain-32338.herokuapp.com/api/users/email-exists', {
			method: 'POST', 
			headers: {
				'Content-Type': 'application/json' 
			},
			body: JSON.stringify({ 
				email:email,
			})

		})
		.then(res => res.json())
		.then(data => {
			if(data === false){
				fetch('https://cryptic-mountain-32338.herokuapp.com/api/users', {
					method:'POST',
					headers: {
						'Content-Type' : 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						password: password1,
						mobileNo: mobileNo
					})

				})
				.then(res => { //additional note: res.send function sets the content type to text/html which means that the client will now treat the response as text. it then retunrs a response to the client.
					return res.json()
				})
				.then(data =>{
					console.log(data)
					//proper response if registration is successful
					if (data === true) {
						alert("new account registered successfully.")
						//redirect to login
						window.location.replace("./login.html")

					} else {
						alert("something went wrong in the registration.")
					}
				})
			}
		})
	} else {
		alert("something went wrong. check credentials.")
	}
})