
let token = localStorage.getItem("token")
let courseId = localStorage.getItem("courseId")
let userId = localStorage.getItem("id")
console.log(courseId)
console.log(userId)



if(!token || token === null){
    alert("Please login")
    window.location.replace("./login.html")

    }else {
        fetch(`https://cryptic-mountain-32338.herokuapp.com/api/courses/${courseId}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
                
            }
            
        })
        .then(res => {
            return res.json()
        })
        .then(data => {
            console.log(data)
            console.log(data.isActive)

            let courseName = data.name
            let courseDescription = data.description
            let homepageDescription2 = data.homepageDescription
            let coursePrice = data.price
       
            if (data.isActive === false) {
                fetch(`https://cryptic-mountain-32338.herokuapp.com/api/courses/`, {
                    method: 'PUT',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${token}`
                        
                    },
                    body:JSON.stringify({
                        courseId:courseId,
                        name: courseName,
                        description: courseDescription,
                        homepageDescription: homepageDescription2,
                        price: coursePrice,
                        isActive: true
                    })
                    
                })
                .then(res => {
                    return res.json()
                })
                .then(data => {
                    console.log(data)
                    
                    alert("course activated.")

                    window.location.replace('./courses.html')
                        //redirect to login
                })
            }else {
                        alert("something went wrong.")
                    }
        })
    }
    