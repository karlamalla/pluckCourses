




let token = localStorage.getItem("token")


	if(!token || token === null) {
        window.location.replace("./login.html")
        alert("Please login first")
    }else {
        fetch('https://cryptic-mountain-32338.herokuapp.com/api/users/details', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
                
            }
        })
        .then(res => {
            return res.json()
        })
        .then(data => {
        	
            console.log(data)
            console.log(data.isAdmin)
            if(data.isAdmin === false){
            	alert("Only admins are allowed to create a course.")
            	window.location.replace("../index.html")

			}else{

				let createCourse = document.querySelector("#createCourse")

	createCourse.addEventListener("submit", (e) => {
		e.preventDefault()

		let courseName = document.querySelector("#courseName").value
		let courseDescription = document.querySelector("#courseDescription").value
		let courseDescription2 = document.querySelector("#courseDescription2").value
		let coursePrice = document.querySelector("#coursePrice").value
		let createdOn= new Date();


				
				fetch('https://cryptic-mountain-32338.herokuapp.com/api/courses', {
					method:'POST',
					headers: {
						'Content-Type' : 'application/json',
						'Authorization': `Bearer ${token}`
					},
					body: JSON.stringify({
						name: courseName,
						description: courseDescription,
						homepageDescription: courseDescription2,
						price: coursePrice,
						createdOn: createdOn
					})

				})
				.then(res => { //additional note: res.send function sets the content type to text/html which means that the client will now treat the response as text. it then retunrs a response to the client.
					return res.json()
				})
				.then(data =>{
					console.log(data)
					//proper response if registration is successful
					if (data === true) {
						alert("new course added successfully.")
						//redirect to login
						window.location.replace("./courses.html")

					} else {
						alert("something went wrong in the registration.")
					}
				})
			})
		}
	})
    }