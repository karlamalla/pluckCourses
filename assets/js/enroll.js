

function enroll(){

    let courseId = document.getElementById("enrollCourse").value
    localStorage.setItem("courseId", courseId)
    
    
    fetch('https://cryptic-mountain-32338.herokuapp.com/api/users/enroll', {
        method:'POST',
            headers: {
                'Content-Type' : 'application/json',
                'Authorization': `Bearer ${token}`
            },
                body: JSON.stringify({
                    userId: userId,
                    courseId: courseId
                })

                })
                .then(res => { //additional note: res.send function sets the content type to text/html which means that the client will now treat the response as text. it then retunrs a response to the client.
                    return res.json()
                })
                .then(data =>{
                    console.log(data)
                    if (data === true) {
                        alert("enrollment successful.")
                        window.location.replace("./profile.html")

                    } else {
                        alert("please login first.")
                        window.location.replace("./login.html")
                    }
                })
            }
        
