
let courseId = localStorage.getItem("courseId")
let userId = localStorage.getItem("id")



let name = document.querySelector("#courseName")
let description = document.querySelector("#courseDescription")
let homepageDescription = document.querySelector("#homepageDescription")
let price = document.querySelector("#coursePrice")


fetch(`https://cryptic-mountain-32338.herokuapp.com/api/courses/${courseId}`)
        .then(res => {
            return res.json()
        })
        .then(data => {
            console.log(data)
            console.log("courseID: " + courseId)
            console.log("userID: " + userId)

            name.placeholder = data.name
            price.placeholder = data.price
            description.placeholder = data.description
            homepageDescription.placeholder = data.homepageDescription

            document.querySelector("#editCourse").addEventListener("submit", (letsedit) => {
                letsedit.preventDefault()

                let courseName = name.value
                let courseDescription = description.value
                let homepageDescription2 = homepageDescription.value
                let coursePrice = price.value
                let token = localStorage.getItem("token")

                console.log("new: " + courseName)
                console.log("new: " + courseDescription)
                console.log("new: " + coursePrice)

                fetch(`https://cryptic-mountain-32338.herokuapp.com/api/courses`, {
                       
                        method:'PUT',
                        headers: {
                            'Content-Type' : 'application/json',
                            'Authorization': `Bearer ${token}`
                            },
                            body: JSON.stringify({
                                courseId: courseId,
                                name: courseName,
                                description: courseDescription,
                                homepageDescription: homepageDescription2,
                                price: coursePrice
                                })

                            })
                            .then(res => { //additional note: res.send function sets the content type to text/html which means that the client will now treat the response as text. it then retunrs a response to the client.
                                return res.json()
                            })
                            .then(data =>{
                                console.log(data)
                                 if (data === true) {
                                   alert("course updated")
                                   window.location.replace("./courses.html")

                                }else {
                                    alert("something went wrong.")
                                }
                            })
                        })
                        })
    